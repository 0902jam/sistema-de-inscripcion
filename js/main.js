/**
 * Fichero Main.js
 * Programacion Estructurada
 */

// Cambio de Ventanas
/**
 * seleccion de los elemento del DOM
 */
let vent1 = document.querySelector("#ven1")
let vent2 = document.querySelector("#ven2")
let vent3 = document.querySelector("#ven3")

let bot1 = document.querySelector("#bot1")
let bot2 = document.querySelector("#bot2")
let bot3 = document.querySelector("#bot3")

/**
 * Funcion que permite el cambio dinamico de ventanas
 * @param {object} el 
 */
function wind(el) {
  if (el === 1) {
    vent1.style = "display: block"
    vent2.style = "display: none"
    vent3.style = "display: none"
  } else if (el === 2) {
    vent2.style = "display: block"
    vent1.style = "display: none"
    vent3.style = "display: none"
  } else if (el === 3) {
    vent3.style = "display: block"
    vent1.style = "display: none"
    vent2.style = "display: none"
  }
} 

/**
 * Asignacion de eventos de ventanas
 */
bot1.addEventListener("click",function() {
  wind(1)
  function recarga() {
    ajax("post","backEnd/main.php", {opc: "2"}, function(response) {
      let elemList = document.querySelector("#tabla1")
      let template1 = `
      <tr class="tabla">
        <th>Cedula de Identidad</th>
        <th>Nombre Completo</th>
        <th>Fecha de Nacimiento</th>
        <th>C.I</th>
        <th>Part. Nac.</th>
        <th>Titulo</th>
        <th>Notas Certif.</th>
        <th>OPSU</th>
        <th>Fondo Negro</th>
      </tr>
      `
      elemList.innerHTML = template1
    
      let template2
      for (let i = 0; i < response.DATA.length; i++) {
        //console.log(response.DATA[i]);
        template2 = `
        <tr class="tabla">
          <td>${response.DATA[i].cedula}</td>
          <td>${response.DATA[i].nombre_completo}</td>
          <td>${response.DATA[i].fecha_nacimiento}</td>
          <td>${response.DATA[i].copia_cedula}</td>
          <td>${response.DATA[i].copia_partida}</td>
          <td>${response.DATA[i].copia_titulo}</td>
          <td>${response.DATA[i].copia_certificacion}</td>
          <td>${response.DATA[i].certificado_opsu}</td>
          <td>${response.DATA[i].fondo_negro}</td>
        </tr>
        `
        elemList.innerHTML += template2   
      }
    })
  }
  recarga()
})

bot2.addEventListener("click",function() {
  wind(2)
  ajax("post","backEnd/main.php", {opc: "2"}, function(response) {
    let elemList = document.querySelector("#tabla3")
    let template
    if (response.ID === "01") {
      //console.log("ventana 2",response)
      template = `<tr><td>${response.DESCRIPCION}</td></tr>`
      elemList.innerHTML = template
    } else {
      //console.log("ventana 2",response)
    
      let template1 = `
        <tr class="tabla">
          <th>Cedula de Identidad</th>
          <th>Nombre Completo</th>
          <th>Fecha de Nacimiento</th>
          <th>C.I</th>
          <th>Part. Nac.</th>
          <th>Titulo</th>
          <th>Notas Certif.</th>
          <th>OPSU</th>
          <th>Fondo Negro</th>
          <th>Acción</th>
        </tr>
      `
      elemList.innerHTML = template1
  
      let template2
      for (let i = 0; i < response.DATA.length; i++) {
        //console.log(response.DATA[i])

        template2 = `
         <tr class="tabla" key="${response.DATA[i].id_aspirante}">
          <td>${response.DATA[i].cedula}</td>
          <td>${response.DATA[i].nombre_completo}</td>
          <td>${response.DATA[i].fecha_nacimiento}</td>
          <td>${(response.DATA[i].copia_cedula === "S") ? `<input class=\"son${response.DATA[i].id_aspirante}\" type=\"checkbox\" chequeado="S"checked disabled>` : `<input class=\"son${response.DATA[i].id_aspirante}\" type=\"checkbox\">`}</td>
          <td>${(response.DATA[i].copia_partida === "S") ? `<input class=\"son${response.DATA[i].id_aspirante}\" type=\"checkbox\" chequeado="S"checked disabled>` : `<input class=\"son${response.DATA[i].id_aspirante}\" type=\"checkbox\">`}</td>
          <td>${(response.DATA[i].copia_titulo === "S") ? `<input class=\"son${response.DATA[i].id_aspirante}\" type=\"checkbox\" chequeado="S"checked disabled>` : `<input class=\"son${response.DATA[i].id_aspirante}\" type=\"checkbox\">`}</td>
          <td>${(response.DATA[i].copia_certificacion === "S") ? `<input class=\"son${response.DATA[i].id_aspirante}\" type=\"checkbox\" chequeado="S"checked disabled>` : `<input class=\"son${response.DATA[i].id_aspirante}\" type=\"checkbox\">`}</td>
          <td>${(response.DATA[i].certificado_opsu === "S") ? `<input class=\"son${response.DATA[i].id_aspirante}\" type=\"checkbox\" chequeado="S"checked disabled>` : `<input class=\"son${response.DATA[i].id_aspirante}\" type=\"checkbox\">`}</td>
          <td>${(response.DATA[i].fondo_negro === "S") ? `<input class=\"son${response.DATA[i].id_aspirante}\" type=\"checkbox\" chequeado="S"checked disabled>` : `<input class=\"son${response.DATA[i].id_aspirante}\" type=\"checkbox\">`}</td>
          <td><input type="button" value="Modificar" class="modif"></td>
         </tr>
        `
        elemList.innerHTML += template2   
      }
    }
  })
  setTimeout(function() {
    let arrayObj = document.querySelectorAll(".modif")
    arrayObj.forEach(x => {
      x.addEventListener("click",function() {
        let elems = this.parentElement.parentNode
        let els = document.querySelectorAll(`.son${elems.getAttribute("key")}`)
                
        let dat1 = (els[0].checked) ? "S" : "N"
        let dat2 = (els[1].checked) ? "S" : "N"
        let dat3 = (els[2].checked) ? "S" : "N"
        let dat4 = (els[3].checked) ? "S" : "N"
        let dat5 = (els[4].checked) ? "S" : "N"
        let dat6 = (els[5].checked) ? "S" : "N"

        let objMod = {
          opc: "4",
          id_aspirante: elems.getAttribute("key"),
          copia_cedula: dat1,
          copia_partida: dat2,
          copia_titulo: dat3,
          /* En copia_certificacion hay un eror de servidor 500, si se eliminan letras de la propiedad retorna */
          /* OJO EN LA PROPIEDAD SIGUIENTE VA EL NOMBRE "copia_certificacion" pero da error */ 
          copia_certificacion: dat4,
          certificado_opsu: dat5,
          fondo_negro: dat6
        }

        console.log(objMod);
        
        ajax("POST","backEnd/main.php", objMod, function(response) {
          console.log(response)
          let notif = document.querySelector("#notif")
      
          if (notif.classList.contains("animated flipOutX") === true || notif.classList.contains("animated flipInX") === true) {
            notif.className = "animated"
          }
          
          notif.className = "animated flipInX"
          notif.style = "display: flex"
        
          if (response.ID === "00") {
            notif.style.background = "#18a15d"
          }
        
          if (response.ID === "00") {
            template = `<p>Datos Modificados</p>`
            notif.innerHTML = template
            notif.style.background = "#18a15d"
          } else {
            template = `<p>${response.DESCRIPCION}</p>`
            notif.innerHTML = template
            notif.style.background = "orangered"
          }
        
          setTimeout(function() {
            notif.className = "animated flipOutX"
          },5000)
        })
      })
    })
  },1000)
})

bot3.addEventListener("click",function() {
  wind(3)
  ajax("post","backEnd/main.php", {opc: "3"}, function(response) {
    let elemList = document.querySelector("#tabla2")
    let template
    if (response.ID === "01") {
      //console.log("ventana 3",response)
      template = `<tr><td>${response.DESCRIPCION}</td></tr>`
      elemList.innerHTML = template
    } else {
      //console.log("ventana 3",response)
    
      let template1 = `
      <tr class="tabla">
        <th>Cedula de Identidad</th>
        <th>Nombre Completo</th>
        <th>Edad</th>
      </tr>
      `
      elemList.innerHTML = template1
      
      let template2
      for (let i = 0; i < response.DATA.length; i++) {
        //console.log(response.DATA[i])
        template2 = `
        <tr class="tabla">
          <td>${response.DATA[i].cedula}</td>
          <td>${response.DATA[i].nombre_completo}</td>
          <td>${response.DATA[i].edad}</td>
        </tr>
        `
        elemList.innerHTML += template2   
      }
    }
  })  
})

// Efecto de Cabecera
let head = document.querySelectorAll("header")[0]
head.children[0].style = "transition: .3s"
head.children[1].style = "transition: .3s"

window.addEventListener("scroll",function() {
  if (window.scrollY > 0) {
    head.children[0].style = "font-size: 0px"
    head.children[1].style = "font-size: 0px"
  } else if (window.scrollY == 0) {
    head.children[0].style = "font-size: 2em"
    head.children[1].style = "font-size: 1.5em"
  }
})

function resetear() {
  let form1 = document.querySelector("#form1")
  let form2 = document.querySelector("#form2")
  form1.reset()
  form2.reset()
}

/**
 * Funcion recolectadora de datos
 * @returns Objeto de valor de los elementos seleccionados del dom
 */
let recolect = function() {
  let cdi = document.querySelector("#cdi")
  let nom = document.querySelector("#nom")
  let ape = document.querySelector("#ape")
  let fen = document.querySelector("#fech")
  let gen = document.querySelector("#gen")

  let dcdi = document.querySelector("#dcdi")
  let dpn = document.querySelector("#dpn")
  let dtb = document.querySelector("#dtb")
  let dnc = document.querySelector("#dnc")
  let dops = document.querySelector("#dops")
  let dfnt = document.querySelector("#dfnt")
  
  return {
    // Opcion
    opc: "1",
    // Datos Iniciales
    cedula: cdi.value,
    nombre: nom.value,
    apellido: ape.value,
    genero: gen.value,
    fecha_nacimiento: fen.value,
    // Documentos consignados
    copia_cedula: (dcdi.checked) ? "S" : "N",
    copia_partida: (dpn.checked) ? "S" : "N",
    copia_titulo: (dtb.checked) ? "S" : "N",
    copia_certificacion: (dnc.checked) ? "S" : "N",
    certificado_opsu: (dops.checked) ? "S" : "N",
    fondo_negro: (dfnt.checked) ? "S" : "N"
  }
}

/**
 * Funcion que serializa el objeto
 * @param {object} obj 
 */
function stringifiar(obj) {
  return JSON.stringify(obj)
}

/**
 * Funcion que permite la asignacion dinamica de mensajes
 * @param {object} obj respuesta del ajax
 */
let notif = function(obj) {
  let notif = document.querySelector("#notif")

  template = `<p>${obj.descripcion}<p>`

  notif.innerHTML = template
}

/**
 * Funcion AJAX que se comunica de manera asincrona con el servido y recibe los datos para tratarlos
 * @param {string} metodo Sea "post", "get", "delete" o "update"
 * @param {string} archivo Ruta del archivo o url de servidor
 * @param {object} obj Objeto de datos en formato JSON
 * @param {function} fun Funcion que debe tener un parametro "response" que gestiona lo que
 * se desea hacer con la informacion que entra por AJAX
 */
function ajax(metodo, archivo, obj, fun = function(response){console.log(response)}) {
  let uso = stringifiar(obj)
  let xhr = new XMLHttpRequest() 
  xhr.onreadystatechange = function() {
    if (this.readyState === 4 && this.status === 200) {
      let resp = this.responseText
      resp = JSON.parse(resp)
      fun(resp)
    }
  }
  xhr.open(metodo,archivo,true)
  xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded")
  xhr.send(`data=${uso}`)
}

// Seleccion del boton de registro
let reg = document.querySelector("#reg")

/**
 * Funcion Proceso que gestiona y muestra la informacion
 */
function proceso() {
  let myobj = recolect()
  console.log(myobj);
    
  ajax("post", "backEnd/main.php", myobj, function(response) {
    let notif = document.querySelector("#notif")
      
    if (notif.classList.contains("animated flipOutX") === true || notif.classList.contains("animated flipInX") === true) {
      notif.className = "animated"
    }
    
    notif.className = "animated flipInX"
    notif.style = "display: flex"

    if (response.ID === "00") {
      notif.style.background = "#18a15d"
    }

    if (response.ID === "00") {
      template = `<p>Usuario Registrado</p>`
      notif.innerHTML = template
      notif.style.background = "#18a15d"
    } else {
      template = `<p>${response.DESCRIPCION}</p>`
      notif.innerHTML = template
      notif.style.background = "orangered"
    }

    setTimeout(function() {
      notif.className = "animated flipOutX"
    },5000)
  })
}

/**
 * Asignacion de evento al boton registro
 */
reg.addEventListener("click",function() {
  proceso()
  setTimeout(function() {
    ajax("post","backEnd/main.php", {opc: "2"}, function(response) {
      let elemList = document.querySelector("#tabla1")
      let template1 = `
      <tr class="tabla">
        <th>Cedula de Identidad</th>
        <th>Nombre Completo</th>
        <th>Fecha de Nacimiento</th>
        <th>C.I</th>
        <th>Part. Nac.</th>
        <th>Titulo</th>
        <th>Notas Certif.</th>
        <th>OPSU</th>
        <th>Fondo Negro</th>
      </tr>
      `
      elemList.innerHTML = template1
    
      let template2
      for (let i = 0; i < response.DATA.length; i++) {
        //console.log(response.DATA[i]);
        template2 = `
        <tr class="tabla">
          <td>${response.DATA[i].cedula}</td>
          <td>${response.DATA[i].nombre_completo}</td>
          <td>${response.DATA[i].fecha_nacimiento}</td>
          <td>${response.DATA[i].copia_cedula}</td>
          <td>${response.DATA[i].copia_partida}</td>
          <td>${response.DATA[i].copia_titulo}</td>
          <td>${response.DATA[i].copia_certificacion}</td>
          <td>${response.DATA[i].certificado_opsu}</td>
          <td>${response.DATA[i].fondo_negro}</td>
        </tr>
        `
        elemList.innerHTML += template2   
      }
    })
  },2000)
  resetear()
})

/**
 * Despliegue de la tabla de la primera vista al cargar la pagina
 */
window.onload = function() {
  ajax("post","backEnd/main.php", {opc: "2"}, function(response) {
    let elemList = document.querySelector("#tabla1")
    let template1 = `
    <tr class="tabla">
      <th>Cedula de Identidad</th>
      <th>Nombre Completo</th>
      <th>Fecha de Nacimiento</th>
      <th>C.I</th>
      <th>Part. Nac.</th>
      <th>Titulo</th>
      <th>Notas Certif.</th>
      <th>OPSU</th>
      <th>Fondo Negro</th>
    </tr>
    `
    elemList.innerHTML = template1
  
    let template2
    for (let i = 0; i < response.DATA.length; i++) {
      //console.log(response.DATA[i]);
      template2 = `
      <tr class="tabla">
        <td>${response.DATA[i].cedula}</td>
        <td>${response.DATA[i].nombre_completo}</td>
        <td>${response.DATA[i].fecha_nacimiento}</td>
        <td>${response.DATA[i].copia_cedula}</td>
        <td>${response.DATA[i].copia_partida}</td>
        <td>${response.DATA[i].copia_titulo}</td>
        <td>${response.DATA[i].copia_certificacion}</td>
        <td>${response.DATA[i].certificado_opsu}</td>
        <td>${response.DATA[i].fondo_negro}</td>
      </tr>
      `
      elemList.innerHTML += template2   
    }
  })
}