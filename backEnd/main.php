<?php
    $data = json_decode($_POST['data']);
    // print_r($data->opc);
    // die();
    $opc = $data->opc;
    switch ($opc) {
        case '1':
            if (isset($data->cedula) && isset($data->nombre) && isset($data->apellido) && isset($data->genero) && isset($data->fecha_nacimiento) && isset($data->copia_cedula) && isset($data->copia_partida) && isset($data->copia_titulo) && isset($data->copia_certificacion) && isset($data->certificado_opsu) && isset($data->fondo_negro)) {
                include 'model/aspiranteModel.php';
                $aspirante = new Aspirante();
                $array = [
                    'cedula' => $data->cedula,
                    'nombre' => $data->nombre,
                    'apellido' => $data->apellido,
                    'genero' => $data->genero,
                    'fecha_nacimiento' => $data->fecha_nacimiento,
                    'copia_cedula' => $data->copia_cedula,
                    'copia_partida' => $data->copia_partida,
                    'copia_titulo' => $data->copia_titulo,
                    'copia_certificacion' => $data->copia_certificacion,
                    'certificado_opsu' => $data->certificado_opsu,
                    'fondo_negro' => $data->fondo_negro,
                ];

                $validar = $aspirante->validar($array);
                if (isset($validar['val'])) {
                    $response = [
                        'ID' => '01',
                        'DESCRIPCION' => 'Revise los datos enviados!!',
                    ];
                } elseif (isset($validar['valid'])) {
                    $response = [
                        'ID' => '02',
                        'DESCRIPCION' => 'Error inesperado',
                    ];
                } elseif (isset($validar['exist'])) {
                    if ($validar['exist']) {
                        $response = [
                            'ID' => '01',
                            'DESCRIPCION' => 'Aspirante ya existe!!',
                        ];
                    } else {
                        if ($aspirante->registrar($array)){
                            $response = [
                                'ID' => '00',
                            ];
                        } else {
                            $response = [
                                'ID' => '02',
                                'DESCRIPCION' => 'Error inesperado',
                            ];
                        }
                    }
                }    
            } else {
                $response = [
                    'ID' => '01',
                    'DESCRIPCION' => 'Faltan datos por favor revise!!',
                ];
            }
            echo json_encode($response);
            break;

        case '2':
            include 'model/aspiranteModel.php';
            $aspirante = new Aspirante();
            $result = $aspirante->getData();
            if (count($result) > 0) {
                $response = [
                    'ID' => '00',
                    'DATA' => $result,
                ];
            } else {
                $response = [
                    'ID' => '01',
                    'DESCRIPCION' => 'No hay estudiantes registrados',
                ];
            }
            echo json_encode($response);
            break;

        case '3':
            include 'model/aspiranteModel.php';
            $aspirante = new Aspirante();
            $result = $aspirante->getData(0);
            if (count($result) > 0) {
                $response = [
                    'ID' => '00',
                    'DATA' => $result,
                ];
            } else {
                $response = [
                    'ID' => '01',
                    'DESCRIPCION' => 'No hay estudiantes que cumplan con los requisitos',
                ];
            }
            echo json_encode($response);
            break;

        case '4':
            include 'model/documentosConsignadosModel.php';

            if (isset($data->copia_cedula) && isset($data->copia_partida) && isset($data->copia_titulo) && isset($data->copia_certificacion) && isset($data->certificado_opsu) && isset($data->fondo_negro)) {
                $array = [
                    'id_aspirante' => $data->id_aspirante,
                    'copia_cedula' => $data->copia_cedula,
                    'copia_partida' => $data->copia_partida,
                    'copia_titulo' => $data->copia_titulo,
                    'copia_certificacion' => $data->copia_certificacion,
                    'certificado_opsu' => $data->certificado_opsu,
                    'fondo_negro' => $data->fondo_negro,
                ];
    
                $documentConsig = new DocumentosConsignados();
                // print_r($array);
                // die();
                
                if($documentConsig->registrar($array, 2)){
                    $response = [
                        'ID' => '00',
                    ];
                } else {
                    $response = [
                        'ID' => '02',
                        'DESCRIPCION' => 'Error inesperado',
                    ];
                }
            } else {
                $response = [
                    'ID' => '01',
                    'DESCRIPCION' => 'Faltan datos por favor revise!!',
                ];
            }

            echo json_encode($response);
            break;

    }


?>