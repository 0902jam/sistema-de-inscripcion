<?php
	/**
     * Clase Aspirante 
     * 
     * @property Int $id identificacion del aspirante
     * @property Int $cedula Numero de cedula del aspirante
     * @property String $nombre indica el nombre o los nombres
     * @property String $apellido indica el apellido o los apellidos
     * @property String $genero Indica si el aspirante es masculino (m) o femenino (f)
     * @property String $fecha_nacimiento Indica la fecha de nacimiento
     */
    class Aspirante
    {
        public $id;
        public $cedula;
        public $nombre;
        public $apellido;
        public $genero;
        public $fecha_nacimiento;

        public function validar($array)
        {
            if (ctype_digit($array['cedula']) && ctype_alpha($array['nombre']) && ctype_alpha($array['apellido'])) {
                include 'BaseDatos.php';
                $db = new BaseDatos();
            
                if ($db->conectar()) {
                    $sql = "SELECT * FROM aspirante WHERE cedula = ". $array['cedula'] ." LIMIT 1";
                    $result = $db->conexion->query($sql);
                    $db->desconectar();
                    if ($result->num_rows > 0) {
                        return [
                            'exist' => TRUE,
                        ];
                    } else {
                        return [
                            'exist' => FALSE,
                        ];
                    }
                } else {
                    return [
                        'valid' => FALSE,
                    ];
                }
            } else {
                return [
                    'val' => FALSE
                ];
            }
        }
        
        public function registrar($array) {
            include '../BaseDatos.php';

            $this->cedula = $array['cedula'];
            $this->nombre = $array['nombre'];
            $this->apellido = $array['apellido'];
            $this->genero = $array['genero'];
            $this->fecha_nacimiento = $array['fecha_nacimiento'];

            $db = new BaseDatos();
            
            if ($db->conectar()) {
                $sql = "INSERT INTO aspirante (cedula, nombre, apellido, genero, fecha_nacimiento) VALUES (". $this->cedula .", \"". $this->nombre ."\", \"". $this->apellido ."\", \"". $this->genero ."\", \"". $this->fecha_nacimiento ."\");";
                if ($db->conexion->query($sql) === TRUE) {
                    include 'model/documentosConsignadosModel.php';
                    $this->id = $db->conexion->insert_id;

                    $arrayDocu = [
                        'id_aspirante' => $this->id,
                        'copia_cedula' => $array['copia_cedula'],
                        'copia_partida' => $array['copia_partida'],
                        'copia_titulo' => $array['copia_titulo'],
                        'copia_certificacion' => $array['copia_certificacion'],
                        'certificado_opsu' => $array['certificado_opsu'],
                        'fondo_negro' => $array['fondo_negro'],
                    ];

                    $documentConsig = new DocumentosConsignados();
                    
                    return $response = ($documentConsig->registrar($arrayDocu)) ? true : false;
                } else {
                    echo "Error: " . $sql . "<br>" . $db->conexion->error;
                    $db->desconectar();
                    return false;
                }   
            }
            return false;
        }

        public function getData($all = 1)
        {
            include 'BaseDatos.php';
            $db = new BaseDatos();
            $data = [];
        
            if ($db->conectar()) {
                if ($all == 1) {                    
                    $sql = "SELECT a.cedula, CONCAT(a.nombre, ' ', a.apellido) as nombre_completo, a.fecha_nacimiento, dc.*  FROM aspirante as a INNER JOIN documentos_consignados as dc ON a.id = dc.id_aspirante";
                } else {
                    $sql = "SELECT a.cedula, CONCAT(a.nombre, ' ', a.apellido) as nombre_completo, TIMESTAMPDIFF(YEAR, a.fecha_nacimiento,CURDATE()) AS edad FROM aspirante as a INNER JOIN documentos_consignados as dc ON a.id = dc.id_aspirante WHERE copia_titulo = 'S' AND certificado_opsu = 'S'";
                }                
                
                $result = $db->conexion->query($sql);
                $db->desconectar();
                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        array_push($data, $row);
                    }
                    return $data;
                }
            } else {
                return [];
            }
        }
    }
    
?>