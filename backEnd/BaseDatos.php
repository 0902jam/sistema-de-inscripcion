<?php
    require 'config.php';
    /**
     * Clase BaseDatos
     */
    class BaseDatos
    {
        public $conexion;

        public function conectar()
        {
            $this->conexion = mysqli_connect(HOST, USER, PASS, DBNAME);
            if (!$this->conexion) {
                return false;
            }
            return true;
        }

        public function desconectar()
        {
            mysqli_close($this->conexion);
        }

    }
    
?>